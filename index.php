<?php


$uri = $_SERVER["REQUEST_URI"];
$method = $_SERVER['REQUEST_METHOD'];

if($uri==="/") {
    echo "<a href=\"/test.html\">Please click here to go to test page</a>";
}
else {
    header('Access-Control-Allow-Origin: *');
    header("Content-type: application/json; charset=utf-8");
    $rest_json = file_get_contents("php://input");
    $parsed_url = parse_url($uri);
    if ($method==='GET' AND $parsed_url['path']==="/user") {
        parse_str($parsed_url["query"], $parsed_query);
        echo json_encode([
            "timestamp" => time(),
            "data" => $parsed_query,
            "error" => [],
        ]);
    }
    else if ($method==='POST' AND $parsed_url['path']==="/user") {
        echo json_encode([
            "timestamp" => time(),
            "data" => json_decode($rest_json),
            "error" => [],
        ]);
    }
    else {
        http_response_code(404);
        echo json_encode([
            "timestamp" => time(),
            "data" => $rest_json,
            "error" => [
                "Unknown request. fail to handle."
            ],
        ]);
    }
}